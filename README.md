# Welcome

**This project was moved!**

Please check out https://gitlab.com/caosdb/caosdb-advanced-user-tools

The sources are located in the "models" submodule.

This is the **CaosDB Model Library** repository and a part of the
CaosDB project.

# Setup

Please read the [README_SETUP.md](README_SETUP.md) for instructions on how to
setup this code.


# Further Reading

Please refer to the [official gitlab repository of the CaosDB
project](https://gitlab.com/caosdb/caosdb) for more information.

# License

Copyright (C) 2018 Research Group Biomedical Physics, Max Planck Institute for
Dynamics and Self-Organization Göttingen.

All files in this repository are licensed under a [GNU Affero General Public
License](LICENCE.md) (version 3 or later).

