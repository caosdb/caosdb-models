# Changelog #

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

Everything, and a bit more:

- Implementation.
- Property sections may be empty.
- Unit tests and integration tests.

### Changed ###

- #2 Error messages now may show line numbers.

### Deprecated ###

### Removed ###

### Fixed ###

### Security ###


### Added
- everything

## [0.1.0] - 2018-11-15
Initial package creation, no source code yet.

### Added
- everything
