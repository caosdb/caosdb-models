# Installation
```
pip3 install . --user
pip3 install tox --user
# or
apt-get install tox
```

# Initialize tox environment #

If tox complains about not finding the `caosdb` module, uncomment the following line from tox.ini,
run tox, then comment that line again:

```ini
commands=
    # pip3 install ../caosdb-pylib
```

# Run Unit Tests
tox

# Code Formatting

autopep8 -i -r ./
