import unittest
from tempfile import NamedTemporaryFile

import caosdb as db

from caosmodels.parser import (
    TwiceDefinedException,
    YamlDefinitionError,
    parse_model_from_yaml,
    parse_model_from_string
    )


def to_file(string):
    f = NamedTemporaryFile(mode="w", delete=False)
    f.write(string)
    f.close()

    return f.name


def parse_str(string):
    parse_model_from_yaml(to_file(string))


def has_property(el, name):
    for p in el.get_properties():
        if p.name == name:
            return True

    return False


def has_parent(el, name):
    for p in el.get_parents():
        if p.name == name:
            return True

    return False


class TwiceTest(unittest.TestCase):
    def test_defined_once(self):
        string = """
RT1:
  recommended_properties:
    a:
RT2:
  recommended_properties:
    RT1:
RT3:
  recommended_properties:
    RT4:
      recommended_properties:
        a:
RT4:
"""
        model = parse_model_from_yaml(to_file(string))
        assert has_property(model["RT1"], "a")
        assert has_property(model["RT4"], "a")

    def test_defined_twice(self):
        string = """
RT1:
  recommended_properties:
    a:
RT2:
  recommended_properties:
    RT1:
      recommended_properties:
        a:
"""

        self.assertRaises(TwiceDefinedException, lambda: parse_model_from_yaml(to_file(string)))

    def test_typical_case(self):
        string = """
RT1:
  recommended_properties:
    p1:
      datatype: TEXT
      description: shiet egal
  obligatory_properties:
    p2:
      datatype: TEXT
RT2:
  description: "This is awesome"
  inherit_from_suggested:
  - RT1
  - RT4
  obligatory_properties:
    RT1:
    p3:
      datatype: DATETIME
  recommended_properties:
    p4:
    RT4:
p1:
p5:
RT5:
  """
        parse_model_from_yaml(to_file(string))

    def test_wrong_kind(self):
        string = """
- RT1:
- RT2:
"""
        self.assertRaises(ValueError, lambda: parse_model_from_yaml(to_file(string)))

    def test_unknown_kwarg(self):
        string = """
RT1:
  datetime:
    p1:
"""
        self.assertRaises(ValueError, lambda: parse_model_from_yaml(to_file(string)))

    def test_definition_in_inheritance(self):
        string = """
RT2:
  description: "This is awesome"
  inherit_from_suggested:
  - RT1:
    description: "tach"
"""
        self.assertRaises(ValueError, lambda: parse_model_from_yaml(to_file(string)))

    def test_inheritance(self):
        string = """
RT1:
  description: "This is awesome"
  inherit_from_suggested:
  - RT2
  inherit_from_recommended:
  - RT3
  inherit_from_obligatory:
  - RT4
  - RT5
RT2:
RT3:
RT4:
RT5:
"""
        model = parse_model_from_yaml(to_file(string))
        assert has_parent(model["RT1"], "RT2")
        assert (model["RT1"].get_parent(
            "RT2")._flags["inheritance"] == db.SUGGESTED)
        assert has_parent(model["RT1"], "RT3")
        assert (model["RT1"].get_parent(
            "RT3")._flags["inheritance"] == db.RECOMMENDED)
        assert has_parent(model["RT1"], "RT4")
        assert (model["RT1"].get_parent(
            "RT4")._flags["inheritance"] == db.OBLIGATORY)
        assert has_parent(model["RT1"], "RT5")
        assert (model["RT1"].get_parent(
            "RT5")._flags["inheritance"] == db.OBLIGATORY)

    def test_properties(self):
        string = """
RT1:
  description: "This is awesome"
  recommended_properties:
    RT2:
  suggested_properties:
    RT3:
  obligatory_properties:
    RT4:
      recommended_properties:
        RT2:
    RT5:
"""
        model = parse_model_from_yaml(to_file(string))
        print(model["RT1"])
        assert has_property(model["RT1"], "RT2")
        assert model["RT1"].get_importance("RT2") == db.RECOMMENDED
        assert has_property(model["RT1"], "RT3")
        assert model["RT1"].get_importance("RT3") == db.SUGGESTED
        assert has_property(model["RT1"], "RT4")
        assert model["RT1"].get_importance("RT4") == db.OBLIGATORY
        assert has_property(model["RT1"], "RT5")
        assert model["RT1"].get_importance("RT5") == db.OBLIGATORY
        assert has_property(model["RT4"], "RT2")
        assert model["RT4"].get_importance("RT2") == db.RECOMMENDED

    def test_datatype(self):
        string = """
p1:
  datatype: TEXT
"""
        parse_model_from_yaml(to_file(string))
        string = """
p2:
  datatype: TXT
"""
        self.assertRaises(ValueError, lambda: parse_model_from_yaml(to_file(string)))

class ListTest(unittest.TestCase):
    def test_list(self):
        string = """
RT1:
  recommended_properties:
    a:
      datatype: LIST(RT2)
RT2:
"""
        model = parse_model_from_yaml(to_file(string))

        # This failed for an older version of caosdb-models
        string_list = """
A:
  obligatory_properties:
    B:
      datatype: LIST(B)
B:
  obligatory_properties:
    c:
      datatype: INTEGER
"""
        model = parse_model_from_yaml(to_file(string_list))

    def test_dmgd_list(self):
        string = """
RT1:
  recommended_properties:
    a:
      datatype: LIST(T2
RT2:
"""
        self.assertRaises(ValueError, lambda: parse_model_from_yaml(to_file(string)))


class ParserTest(unittest.TestCase):
    """Generic tests for good and bad syntax."""
    def test_empty_property_list(self):
        """Emtpy property lists are allowed now."""
        empty = """
A:
  obligatory_properties:
"""
        parse_str(empty)

    def test_non_string_name(self):
        """Test for when the name does not look like a string to YAML."""
        name_int = """1:
  recommended_properties:
    1.2:
    Null:
    0x0:
    010:
"""
        model = parse_model_from_string(name_int)
        self.assertEqual(len(model), 5)
        for key in model.keys():
            self.assertIsInstance(key, str)

    def test_unexpected_keyword(self):
        """Test for when keywords happen at places where they should not be."""
        yaml = """A:
  obligatory_properties:
    recommended_properties:
"""
        with self.assertRaises(YamlDefinitionError) as yde:
            parse_model_from_string(yaml)
        self.assertIn("line 3", yde.exception.args[0])
        self.assertIn("recommended_properties", yde.exception.args[0])

    def test_parents_list(self):
        """Parents must be a list."""
        yaml = """A:
  inherit_from_obligatory:
    A:
"""
        with self.assertRaises(YamlDefinitionError) as yde:
            parse_model_from_string(yaml)
        self.assertIn("line 3", yde.exception.args[0])


class ExternTest(unittest.TestCase):
    """TODO Testing the "extern" keyword in the YAML."""
    @unittest.expectedFailure
    def test_extern(self):
        raise NotImplementedError("Extern testing is not implemented yet.")


class ErrorMessageTest(unittest.TestCase):
    """Tests for understandable error messages."""
    def test_non_dict(self):
        """When a value is given, where a list or mapping is expected."""
        recordtype_value = """
A: "some class"
"""
        recommended_value = """
A:
  recommended_properties: 23
"""
        property_value = """
prop:
  datatype: DOUBLE
A:
  recommended_properties:
  - prop: 3.14
"""
        # Failing strings and the lines where they fail
        failing = {
            recordtype_value: 2,
            recommended_value: 3,
            property_value: 6
        }
        for string, line in failing.items():
            # parse_str(string)
            with self.assertRaises(YamlDefinitionError) as yde:
                parse_str(string)
            assert("line {}".format(line) in yde.exception.args[0])
